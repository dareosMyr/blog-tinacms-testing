import ReactMarkdown from "react-markdown";
import { InlineWysiwyg } from "react-tinacms-editor";

export default function PostBody({ content }) {
  return (
    <div className="max-w-2xl mx-auto">
      <InlineWysiwyg name="content" format="markdown">
        <ReactMarkdown source={content} />
      </InlineWysiwyg>
    </div>
  );
}
