import "../styles/index.css";
import {
  StrapiMediaStore,
  StrapiProvider,
  StrapiClient,
} from "react-tinacms-strapi";
import { useCMS } from "@tinacms/react-core";
import { TinaCMS, TinaProvider } from "tinacms";
import { useMemo } from "react";

export default function MyApp({ Component, pageProps }) {
  const cms = useMemo(
    () =>
      new TinaCMS({
        enabled: pageProps.preview,
        toolbar: pageProps.preview,
        apis: {
          strapi: new StrapiClient(process.env.STRAPI_URL),
        },
        media: new StrapiMediaStore(process.env.STRAPI_URL),
      }),
    []
  );
  return (
    <TinaProvider cms={cms}>
      <StrapiProvider
        onLogin={() => {
          enterEditMode;
        }}
        onLogout={() => {
          exitEditMode;
        }}
      />
      <EditButton />
      <Component {...pageProps} />
    </TinaProvider>
  );
}

export const EditButton = () => {
  const cms = useCMS();
  return (
    <button
      onClick={() => {
        cms.enabled ? cms.disable() : cms.enable();
      }}
    >
      {cms.enabled ? "Stop Editing" : "Edit this Site"}
    </button>
  );
};

const enterEditMode = () => {
  return fetch(`/api/preview`).then(() => {
    window.location.href = window.location.pathname;
  });
};

const exitEditMode = () => {
  return fetch(`/api/reset-preview`).then(() => {
    window.location.reload();
  });
};
